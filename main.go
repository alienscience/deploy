package main

import (
	"crypto/sha256"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/rjeczalik/notify"
)

const (
	IncomingDir    = "inbox"
	ChecksumSuffix = ".sha256sum"
	RetryDelay     = 2 * time.Minute
)

type Deployer struct {
	changedFilesCh chan notify.EventInfo
	retryCh        chan string
	debounce       map[string]time.Time
	isDryRun       bool
	isOneShot      bool
	isCleanup      bool
}

func NewDeployer(isDryRun, isOneShot, isCleanup bool) *Deployer {
	return &Deployer{
		changedFilesCh: make(chan notify.EventInfo, 256),
		retryCh:        make(chan string, 8),
		debounce:       make(map[string]time.Time),
		isDryRun:       isDryRun,
		isOneShot:      isOneShot,
		isCleanup:      isCleanup,
	}
}

func (d *Deployer) Run() {
	if d.isOneShot {
		d.runOnce()
	} else {
		d.runAsDaemon()
	}
}

func (d *Deployer) runOnce() {
	entries, err := os.ReadDir(IncomingDir)
	if err != nil {
		log.Printf("Unable to read %s: %s", IncomingDir, err)
		return
	}
	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}
		path := filepath.Join(IncomingDir, entry.Name())
		d.handleChangedFile(path)
	}
}

func (d *Deployer) runAsDaemon() {
	for {
		ev := <-d.changedFilesCh
		path := ev.Path()
		d.handleChangedFile(path)
	}
}

func (d Deployer) tryDeploy(path string) {
	go func() {
		for i := 0; i < 6; i++ {
			time.Sleep(time.Duration(i) * RetryDelay)
			if d.deploy(path) {
				break
			}
		}
	}()
}

func (d *Deployer) handleChangedFile(path string) {
	if !strings.HasSuffix(path, ChecksumSuffix) {
		return
	}
	log.Print("checking ", path)
	if !d.checksumOK(path) {
		return
	}
	last := d.debounce[path]
	if time.Since(last) < 30*time.Second {
		log.Printf("file %s has recently changed - ignoring", path)
		return
	}
	d.debounce[path] = time.Now()
	d.tryDeploy(path)
}

func (d Deployer) checksumOK(path string) bool {
	checksumFile, err := os.Open(path)
	if err != nil {
		log.Print("checksum error: ", err)
		return false
	}
	var checksum string
	n, err := fmt.Fscan(checksumFile, &checksum)
	if err != nil {
		log.Print("checksum error: ", err)
		return false
	}
	if n != 1 {
		log.Print("checksum error: failed to read checksum from file")
		return false
	}
	archive := strings.TrimSuffix(path, ChecksumSuffix)
	archiveFile, err := os.Open(archive)
	if err != nil {
		log.Print("checksum error: ", err)
		return false
	}
	defer archiveFile.Close()
	h := sha256.New()
	_, err = io.Copy(h, archiveFile)
	if err != nil {
		log.Print("checksum error: ", err)
		return false
	}
	archiveChecksum := fmt.Sprintf("%x", h.Sum(nil))
	if archiveChecksum != checksum {
		log.Printf("checksum failure: wanted %s, got %s", checksum, archiveChecksum)
		return false
	}
	return true
}

func (d Deployer) deploy(path string) bool {
	archive := strings.TrimSuffix(path, ChecksumSuffix)
	cwd, err := os.Getwd()
	if err != nil {
		log.Print("deploy error: ", err)
		return false
	}
	if !filepath.IsAbs(archive) {
		archive = filepath.Join(cwd, archive)
	}
	tmpDir, err := os.MkdirTemp("", "deploy")
	if err != nil {
		log.Print("deploy error: ", err)
		return false
	}
	if d.isCleanup {
		defer os.RemoveAll(tmpDir)
	}
	err = d.runCommand(tmpDir, "tar", "-axf", archive)
	if err != nil {
		log.Print("Deploy Error:", err)
		return false
	}
	err = d.runCommand(tmpDir, "sudo", "./install")
	if err != nil {
		log.Print("deploy error: ", err)
		return false
	}
	log.Print("deployed ", archive)
	return true
}

func (d Deployer) runCommand(dir string, name string, args ...string) error {
	var cmd *exec.Cmd
	if d.isDryRun {
		args = append([]string{name}, args...)
		cmd = exec.Command("echo", args...)
	} else {
		cmd = exec.Command(name, args...)
	}
	cmd.Dir = dir
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	return cmd.Run()
}

func main() {
	log.SetFlags(0)
	dryrun := flag.Bool("dryrun", false, "Do not deploy, just print the command")
	oneshot := flag.Bool("oneshot", false, "Do not wait for changes, deploy all then exit")
	cleanup := flag.Bool("cleanup", true, "clean up temporary files after deploy")
	flag.Parse()
	deployer := NewDeployer(*dryrun, *oneshot, *cleanup)
	if !*oneshot {
		err := notify.Watch(IncomingDir, deployer.changedFilesCh, notify.Write)
		if err != nil {
			log.Fatal(err)
		}
		defer notify.Stop(deployer.changedFilesCh)
	}
	deployer.Run()
}
