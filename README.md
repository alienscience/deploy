
# Deploy

Daemon that deploys tar files.

The daemon looks for a subdirectory `inbox` of the current working directory. The program waits for a file ending with `.sha256` to be written e.g:

```
my-project.tar.zst.sha256
```

If another file exists, and has the correct hash matching the one in the `.sha256` file, the other file is assumed to be a tar file and is expanded into a temporary directory. After expansion, if a file `install` exists, this file is run to install the program and supporting files.

e.g:

```
my-project.tar.zst  # expands tar file and runs 'install'
my-project.tar.zst.sha256
```
