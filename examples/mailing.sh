#!/bin/bash
set -e

# Copy files
mkdir -p /etc/mailing
cp mailing.toml /etc/mailing

cp mailin /usr/local/bin/
chmod a+x /usr/local/bin/mailin

cp mailing.service /etc/systemd/system/
chmod a+r /etc/systemd/system/mailing.service
cp mailing.conf /etc/nginx/conf.d/
chmod a+r /etc/nginx/conf.d/mailing.conf

# Create user
/usr/bin/getent passwd mailing || {
    /usr/sbin/useradd -r -d /home/mailing -s /sbin/nologin mailing
    /usr/bin/chown -R mailing:mailing /home/mailing
}
/usr/bin/mkdir -p /var/mailing
/usr/bin/chown mailing:mailing /var/mailing

# Certs
[ -f /etc/mailing/fullchain.pem -a -f /etc/mailing/privkey.pem ] || {
    certbot -n --nginx -d mail.alienscience.org --redirect
    cp /etc/letsencrypt/live/mail.alienscience.org/fullchain.pem /etc/mailing/
    cp /etc/letsencrypt/live/mail.alienscience.org/privkey.pem /etc/mailing/
    chgrp mailing /etc/mailing/*.pem
    chmod g+r /etc/mailing/*.pem
}

# Restart
/usr/bin/systemctl daemon-reload
/usr/bin/systemctl restart mailing
/usr/bin/systemctl enable mailing
/usr/sbin/nginx -s reload
